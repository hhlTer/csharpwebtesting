﻿using System;
using System.ComponentModel;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;

namespace ConsoleApp1
{
    //Deprecated
    internal class Driver
    {
        private static IWebDriver _driver;
        
        private const string DefaultPath = @"c:\Users\V-Hruzytskyi-pc\RiderProjects\ConsoleApp1\Drivers\";
        private const string Path = DefaultPath;

        private Driver()
        {
        }
        
        private Driver(Browser browser)
        {
            _driver = CreateDriver(browser);
        }

        public static IWebDriver CreateDriver(Browser browser)
        {
            if (_driver != null)
            {
                return _driver;
            }
            
            if (browser == Browser.Chrome)
            {
                // return new ChromeDriver();
                return new ChromeDriver(ChromeDriverService.CreateDefaultService(Path));
            }
            else
            {
                return new FirefoxDriver(FirefoxDriverService.CreateDefaultService(Path));
            }
            
        }

        [Serializable]
        internal enum Browser
        {
            [Description("Google Chrome")]
            Chrome,
            [Description("Mozilla Firefox")]
            Firefox
        }
    }
}