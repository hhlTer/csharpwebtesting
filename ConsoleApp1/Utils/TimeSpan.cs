﻿namespace ConsoleApp1
{
    public class TimeSpan
    {
        internal static int FromSeconds(int sec)
        {
            return sec * 1000;
        }

        internal static int FromMilliseconds(int milliseconds)
        {
            return milliseconds;
        }
    }
}