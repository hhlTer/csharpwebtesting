﻿using System;
using System.Threading;

namespace ConsoleApp1
{
    public static class Executor
    {
        internal static bool SpinWait(Func<bool> func, int waitTime, int period)
        {
            while (waitTime > period)
            {
                if (func.Invoke())
                {
                    return true;
                }

                // System.Threading.SpinWait(func, period, waitTime);
                System.Threading.Thread.Sleep(System.TimeSpan.FromMilliseconds(period));
                period += period;
            }
            
            return false;
        }
    }
}